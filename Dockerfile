FROM nginx:stable

EXPOSE 8000
COPY nginx.conf /etc/nginx/nginx.conf
